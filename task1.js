// Task 1: Translate the story into code.
const myBirthday = (isKayoSick) => {
    return new Promise((res, rej) => {
        setTimeout(()=>{
            if(!isKayoSick){
                res('Hello')
            } else {
                rej(new Error('I am sad'))
            }
        },2000)
    })
}

console.time('Timer');

myBirthday(false)
.then(result => {
    console.timeLog('Timer');
    console.log(' i have ', result);
})
.catch(err => {
    console.timeLog('Timer');
    console.log(err);
})
.finally(() => console.log('Party'))